#Courbe de Lorenz sur la variables des prix
#Sélection du sous-échantillon de travail que l'on appelle price
#Tri des individus dans l'ordre croissant des valeurs de la variable,
price = data['price'].sort_values().values

#On place les observations dans une variable
#Calcul de la somme cumulée et normalisation en divisant par la somme des observations
lorenz_price = np.cumsum(price) / price.sum() 
plt.figure(figsize=(8,5))
plt.plot(np.linspace(0,1,len(lorenz_price)), lorenz_price, drawstyle='steps-post', color='rosybrown', label='Lorenz')
plt.fill_between(np.linspace(0,1,len(lorenz_price)) ,lorenz_price , color='#539ecd')
plt.plot([0, 1], [0, 1], 'r-', lw=2, label='Distribution égalitaire')
plt.vlines(x=.76, ymin=0, ymax=.5, color='blue', linestyle='--', linewidth=1, label='Medial')
plt.hlines(xmin=.76, xmax=0, y=.5, color='blue', linestyle='--', linewidth=1)

plt.title('Courbe de Lorenz des prix de vente')
plt.xlabel("Distribution des ventes (%)")
plt.ylabel("Cumul des prix de ventes (%)")
plt.legend(loc="best")

plt.show()